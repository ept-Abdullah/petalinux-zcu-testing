# Welcome to the Lassen Peak build!

## _Overview_

This package has everything you need to work with the build except:
- PetaLinux Tools 2022.1

If you need any help installing petalinux feel free to look at:
https://www.zachpfeffer.com/single-post/Download-and-Install-Xilinxs-20174-PetaLinux-Tools

## __How things Work__
  ```

Overall you'll:
1. Fetch code 
2. Setup the Environment
3. Getting Started with the build
4. Load the Build with jtag
```

### __Fetch code __

1. Fetch the code from Git repo with submodules
``` sh
git clone git@gitlab.com:ept-Abdullah/petalinux-zcu-testing.git
```

### __Setup the Environment__

1. Start by setting up the environment:

For setting petalinux enviroment , just hit below command in terminal:
``` sh
Source /path-to_petalinux_setup_installation/settings.sh
```

### __Getting started with the build__
For petalinux build , just hit below command in terminal:

1. Build any package like
``` sh
$ petalinux-build -c kernel
```
or complete build can be done
``` sh
$ petalinux-build
```
### __Load the Build with jtag__
 For petalinux boot with jtag just hit below command:
 ``` sh
 petalinux-boot --jtag --kernel 
```


## References
1. https://www.xilinx.com/content/dam/xilinx/support/documents/boards_and_kits/zcu104/xtp482-zcu104-quickstart.pdf (Hardware)
2. https://docs.xilinx.com/v/u/en-US/ug1267-zcu104-eval-bd (hardware)
3. https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842475/PetaLinux+Yocto+Tips (Peta linux)
